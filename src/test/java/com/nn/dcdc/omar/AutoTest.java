package com.nn.dcdc.omar;

import org.junit.Test;

public class AutoTest {
    Auto porsche = new Auto();

    @org.junit.Test
    public void getSnelheid() {

        int sn = porsche.getSnelheid();
        System.out.println("De snelheid is: " + sn);


    }

    @Test
    public void gaHarder() {

        int sn = porsche.getSnelheid();
        System.out.println("De snelheid is: " + sn);
        porsche.gaHarder(50);
        System.out.println("De snelheid is: " + porsche.getSnelheid());
    }

    @Test
    public void gaLangzamer() {
        int sn = porsche.getSnelheid();
        System.out.println("De snelheid is: " + sn);
        porsche.gaLangzamer(50);
        System.out.println("De snelheid is: " + porsche.getSnelheid());
    }
}